import React from "react";

const Appointment = props => {
  return (
    <div>
      <h3>{props.appointment.title}</h3>
      <p>{props.appointment.appt_date}</p>
    </div>
  );
};

export default Appointment;
