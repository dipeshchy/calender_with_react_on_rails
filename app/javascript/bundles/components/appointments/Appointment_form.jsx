import React, { Component } from "react";

class Appointment_form extends Component {
  
  handleChange = e => {
    let obj = {}
    obj[e.target.name] =  e.target.value;
    this.props.onUserInput(obj);
  }


  render() {
    return (
      <div>
        <h3>Make an Appointment</h3>
          <form className="form-group">
            <label>Title:</label>
            <input
              type="text"
              name="title"
              defaultValue={this.props.title}
              className="form-control"
              onChange={this.handleChange}
            />
            <label>Appointment Time:</label>
            <input
              type="text"
              name="appt_date"
              defaultValue={this.props.appt_date}
              className="form-control"
              onChange={this.handleChange}
            />
            <input
              type="submit"
              value="Make an Appointment"
              className="btn btn-success mt-2"
            />
          </form>
      </div>
    );
  }
}

export default Appointment_form;
