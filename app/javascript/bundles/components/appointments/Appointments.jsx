import React, { Component } from "react";
import Appointment from "./Appointment";
import Appointment_form from "./Appointment_form";

class Appointments extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      appt_date: "Tommorow at 9am"
    };
  }

  handleUserInput = obj => {
    this.setState(obj);
    console.log(this.state.title)
  };

  // handleChange = e => {
  //   this.setState = {
  //     [e.target.name]: e.target.value
  //   }
  //   alert(this.state.title)
  // }

  render() {
    return (
      <div className="container">
        <h1>Appointments</h1>
        <div className="mb-5">
          <Appointment_form onUserInput={this.handleUserInput} title={this.state.title} appt_date={this.state.appt_date} />
          {/* <h3>Make an Appointment</h3>
          <form className="form-group">
            <label>Title:</label>
            <input
              type="text"
              name="title"
              defaultValue={this.state.title}
              className="form-control"
              onChange={this.handleChange}
            />
            <label>Appointment Time:</label>
            <input
              type="text"
              name="appt_date"
              defaultValue={this.state.appt_date}
              className="form-control"
              onChange={this.handleChange}
            />
            <input
              type="submit"
              value="Make an Appointment"
              className="btn btn-success mt-2"
            />
          </form> */}
        </div>
        <div className="card">
          <h3>Appointments List</h3>
          {this.props.appointments.map(function(appointment) {
            return (
              <div key={appointment.id}>
                <Appointment appointment={appointment} />
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

export default Appointments;
