class AppointmentsController < ApplicationController
  def index
    @appointments = Appointment.order('appt_date ASC')
    @appointment = Appointment.new
  end

  def create
    @appointment = Appointment.create(appointment_params)
    # redirect_to :root
    @appointments = Appointment.order('appt_date ASC')
  end


  private
  def appointment_params
    params.require(:appointment).permit(:title,:appt_date)
  end
end
